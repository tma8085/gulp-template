const gulp = require('gulp')
const pug2html = require('./gulp/pug2html')
const styles = require('./gulp/styles')
const server = require('browser-sync').create()
const livereload = require('gulp-livereload')

function readyReload(cb) {
  server.reload()
  cb()
}

function serve(cb) {
  server.init({
    server: 'build',
    notify: false,
    open: true,
    cors: true
  })
  livereload.listen();
  gulp.watch('src/**/*.pug', gulp.series(pug2html))
  gulp.watch('src/**/*.scss', gulp.series(styles))
  return cb()
}

// gulp.task('start',gulp.series(gulp.parallel(pug2html,styles)))
gulp.task('start', gulp.parallel(pug2html,styles,serve))
