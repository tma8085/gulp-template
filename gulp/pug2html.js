const gulp = require('gulp')
const plumber = require('gulp-plumber')
const pugLinter = require('gulp-pug-linter')
const pug = require('gulp-pug')
const livereload = require('gulp-livereload')

module.exports = function pug2html() {
  return gulp.src('src/pages/*.pug')
    .pipe(plumber())
    .pipe(pugLinter({reporter: 'default'}))
    .pipe(pug())
    .pipe(gulp.dest('build'))
    .pipe(livereload())
}